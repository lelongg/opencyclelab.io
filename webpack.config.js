const path = require("path");
module.exports = {
  entry: './src/index.js',
  watch: true,
  watchOptions: {
    aggregateTimeout: 300,
    poll: 1000
  },
  output: {
    filename: 'app.js',
    path: path.resolve(__dirname, 'public'),
    publicPath: "/public/",
  },
  resolve: {
    extensions: ['.js', '.vue', '.css', '.json'],
    alias: {
      root: path.join(__dirname, 'src'),
      components: path.join(__dirname, 'src/components')
    }
  },
  module: {
      loaders: [
          {
              test: /\.scss$/,
              loaders: ['style-loader', 'css-loader', 'sass-loader']
          },
          {
              test: /\.css$/,
              loader: ['style-loader', 'css-loader']
          },
          {
              test: /\.vue$/,
              loaders: ['vue-loader']
          },
          {
              test: /\.js$/,
              loaders: ['babel-loader'],
              exclude: [/node_modules/]
          }
      ]
  }
};

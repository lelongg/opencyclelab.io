import { fetchFavorites } from './local_storage'
import services from '../services'

const actions = {
  changeStation({ commit, state }, data)
  {
    commit('CHANGE_STATION', data)
  },
  changeFavorite({ commit, state }, data)
  {
    commit('CHANGE_FAVORITE', data)
  },
  toggleStation({ commit, state }, data)
  {
    commit('TOGGLE_STATION', data)
  },
  nextStation({ commit, state }, data)
  {
    commit('NEXT_STATION', data)
  },
  previousStation({ commit, state }, data)
  {
    commit('PREVIOUS_STATION', data)
  },
  loadFavorites(state)
  {
    if (!state.favorites || Object.keys(state.favorites).length === 0)
    {
      return fetchFavorites().then(res =>
      {
        let favorites = [];
        Object.keys(res).forEach(key =>
        {
          favorites.push(res[key])
        })
        state.commit('SET_FAVORITES', favorites);
      });
    }
  },
  loadBicycleServices( {commit, state} )
  {
    for (let service_name in services)
    {
      let promise = services[service_name]
      var markers = []

      promise.then(service_info =>
      {
        commit('NEW_BICYCLE_SERVICE', service_info)
      })
    }
  },
  loadUserPosition( {commit, state} )
  {
    if (navigator.geolocation)
    {
      navigator.geolocation.getCurrentPosition(data =>
      {
        var position = {
          lattitude: data.coords.latitude,
          longitude: data.coords.longitude
        };
        commit('USER_POSITION', position)
      }, () =>
      {
        commit('NO_USER_POSITION')
      });
    }
    else
    {
      commit('NO_USER_POSITION')
    }
  }
}

export { actions }

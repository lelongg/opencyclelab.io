import store from './index'

let deg2Rad = (deg) =>
{
  return deg * Math.PI / 180;
}

let pythagorasEquirectangular = (lat1, lon1, lat2, lon2) =>
{
  lat1 = deg2Rad(lat1);
  lat2 = deg2Rad(lat2);
  lon1 = deg2Rad(lon1);
  lon2 = deg2Rad(lon2);
  let R = 6371;
  let x = (lon2 - lon1) * Math.cos((lat1 + lat2) / 2);
  let y = (lat2 - lat1);
  let d = Math.sqrt(x * x + y * y) * R;
  return d;
}

let nearestStation = (latitude, longitude, stations) =>
{
  let mindif = 99999;
  let closest;

  for (let key in stations)
  {
    let position = stations[key].position
    let dif = pythagorasEquirectangular(latitude, longitude, position.lattitude, position.longitude);
    if (dif < mindif)
    {
      closest = key;
      mindif = dif;
    }
  }

  return closest;
}

export { nearestStation }

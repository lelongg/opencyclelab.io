import Vue from 'vue'
import Vuex from 'vuex'

import { mutations } from './mutations'
import { actions } from './actions'

Vue.use(Vuex)

const state = {
  services: {},
  stations: {},

  current_station: null,

  favorite: {
    items: [],
    index: -1,
  },

  user_position: null,
  center_position: {lattitude: 45.7695698, longitude: 3.0965976}
}

const store = new Vuex.Store({
  state,
  mutations,
  actions
})

export default store
